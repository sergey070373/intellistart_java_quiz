package com.example.dbProject.controller;

import com.example.dbProject.dto.ProductCreateDTO;
import com.example.dbProject.entity.Product;
import com.example.dbProject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductRestController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public Collection<Product> getProductList() {
        return productService.getList();
    }

    @DeleteMapping(value = "/{id}")
    public Integer deleteProduct(@PathVariable Integer id) {
        return productService.deleteProduct(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Integer createProduct(@RequestBody @Validated ProductCreateDTO productCreateDTO) {
        return productService.createProduct(productCreateDTO);
    }

    @GetMapping(value = "{id}")
    public Product getProduct(@PathVariable Integer id) {
        return productService.getProduct(id);
    }



}