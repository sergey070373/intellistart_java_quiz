package com.example.dbProject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class    UserCreateDTO {
    private String firstName;
    private String lastName;
    private Integer totalMoney;
}

