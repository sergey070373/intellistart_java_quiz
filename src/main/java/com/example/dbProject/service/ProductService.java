package com.example.dbProject.service;

import com.example.dbProject.dto.ProductCreateDTO;
import com.example.dbProject.entity.Product;

import java.util.Collection;

public interface ProductService {
        Collection<Product> getList();

        Integer createProduct(ProductCreateDTO productCreateDTO);

        Integer deleteProduct(Integer id);

        Product getProduct(Integer productId);
}
