package com.example.dbProject.service;

import com.example.dbProject.dto.UserCreateDTO;
import com.example.dbProject.dto.UserUpdateDTO;
import com.example.dbProject.entity.User;
import com.example.dbProject.exception.UserNotFoundException;
import com.example.dbProject.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ProductService productService;

    public UserServiceImpl(UserRepository userRepository, ProductService productService) {
        this.userRepository = userRepository;
        this.productService = productService;
    }

    @Override
    public Collection<User> getList() {

        return (Collection<User>) userRepository.findAll();
    }

    @Override
    public Integer deleteUser(Integer id) {
        userRepository.deleteById(id);
        return id;
    }

    @Override
    public Integer createUser(UserCreateDTO userDTO) {
        return userRepository.save(new User(userDTO)).getId();
    }

    @Override
    public User getUser(Integer userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }
    @Override
    public User updateUser(Integer id, UserUpdateDTO userNew) {
        User user = userRepository.findById(id).get();
        if (Objects.nonNull(userNew.getFirstName()) && !user.getFirstName().isEmpty()) {
            user.setFirstName(userNew.getFirstName());
        }
        if (Objects.nonNull(userNew.getLastName()) && !user.getLastName().isEmpty()) {
            user.setLastName(userNew.getLastName());
        }

        if (Objects.nonNull(userNew.getTotalMoney()) && user.getTotalMoney()>=0) {
            user.setTotalMoney(userNew.getTotalMoney());
        }
    return userRepository.save(user);
    }

    @Override
    public User updateUserProperty(Integer id, UserUpdateDTO userDTO2) {
        User user = userRepository.findById(id).get();
        if (userDTO2.getFirstName() != null) {
            user.setFirstName(userDTO2.getFirstName());
        }
        if (userDTO2.getLastName() != null) {
            user.setLastName(userDTO2.getLastName());
        }

        if (userDTO2.getTotalMoney() != null) {
            user.setTotalMoney(userDTO2.getTotalMoney());
        }

        if (userDTO2.getFirstName() != null && userDTO2.getTotalMoney() != null) {
            user.setFirstName(userDTO2.getFirstName());
            user.setTotalMoney(userDTO2.getTotalMoney());
        }
      return userRepository.save(user);
    }
    @Override
    public Integer deleteProductFromUser(Integer userId, Integer productId) {
        User user = getUser(userId);
        user.getProducts().remove(productService.getProduct(productId));
        userRepository.save(user);
        return productId;
    }

    @Override
    public Integer addProductToUser(Integer userId, Integer productId) {
        User user = getUser(userId);
        user.getProducts().add(productService.getProduct(productId));
        userRepository.save(user);
        return productId;
    }
}

